package primeraapp4b.facci.melissamendoza.prueba.miprimeraapp4b;

import android.app.Dialog;
import android.app.Notification;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import static primeraapp4b.facci.melissamendoza.prueba.miprimeraapp4b.R.id.btnAutenticar;

public class ActividadPrincipal extends AppCompatActivity {
    Button buscar;
    Button login;
    Button registrar;
    Button PasarParametro;
    Button Fragmentos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_principal);

        buscar = (Button)findViewById(R.id.button3);

        buscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent buscar =new Intent(ActividadPrincipal.this, ActividadBuscar.class);
                startActivity(buscar);
            }
        });

        login = (Button)findViewById(R.id.button);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent login =new Intent(ActividadPrincipal.this, ActividadLogin.class);
                startActivity(login);
            }
        });

        registrar = (Button)findViewById(R.id.button2);

        registrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent registrar =new Intent(ActividadPrincipal.this, ActividadRegistrar.class);
                startActivity(registrar);
            }
        });
        PasarParametro = (Button)findViewById(R.id.button7);

        PasarParametro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent registrar =new Intent(ActividadPrincipal.this, ActividadPasarParametro.class);
                startActivity(registrar);
            }
        });
        Fragmentos = (Button)findViewById(R.id.button8);

        Fragmentos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent registrar =new Intent(ActividadPrincipal.this, fragmentos.class);
                startActivity(registrar);
            }
        });

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.opcionLogin:
                Dialog dialogLogin = new Dialog(ActividadPrincipal.this);
                dialogLogin.setContentView(R.layout.dlg_login);
                Button botonAutenticar = (Button) dialogLogin.findViewById(btnAutenticar);
                final EditText cajaUsuario = (EditText) dialogLogin.findViewById(R.id.txtUser);
                final EditText cajaClave = (EditText) dialogLogin.findViewById(R.id.txtPassword);

                botonAutenticar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Toast.makeText(ActividadPrincipal.this, cajaUsuario.getText().toString()+ " " + cajaClave.getText().toString(),Toast.LENGTH_LONG).show();
                    }
                });
                dialogLogin.show();
                //intent = new Intent(MainActivity.this, login.class);
                //startActivity(intent);
                break;
            case R.id.opcionRegistrar:
                break;
        }
        return true;
    }
}
